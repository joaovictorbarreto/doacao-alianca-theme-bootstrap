<html>

<?php error_reporting(0); ?>

<head>
  <title> Aliança de Misericórdia | Doe Agora </title>

  <link rel="stylesheet" href="style.css" />
  <!--<link rel="stylesheet" href="bootcss/reset.css" />-->
  <link rel="stylesheet" href="bootcss/bootstrap/bootstrap.css" />
  <link rel="styleshhet" href="bootcss/fonticon.css" />
  <meta name="viewport" content="width=device-width,initial-scale=1, user-scalable=no">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nightingale">
  
  
  <!--[if lt IE 9]>
            <script src="_cdn/html5shiv.js"></script>
  <![endif]-->
</head>

<body>
  <div class="bg_header"> <img src="images/bg_header.png" /></div>
  <div class="main_header content">
    
    <img src="images/logo.png" class="main_logo" />
    <div class="content">
        <div class="main_header_bar">
            <div class="main_header_bar_line contact">
                <div class="main_header_bar_contact"><a title="Fale Conosco" class="jwc_contact fc" href="#">Fale Conosco</a><br></li>(11) 3120-9157 <br><a href="mailto:" title="Enviar E-mail">maisalianca@misericordia.com.br</a></div>
                <ul class="main_header_bar_social">
                    <?php
                    if (SITE_SOCIAL_FB):
                        echo '<li><a class="" target="_blank" href="https://www.facebook.com/' . SITE_SOCIAL_FB_PAGE . '" title="No Facebook"><img src="images/icon/icon-fb.png" alt="' . SITE_NAME . ' - Facebook" title="' . SITE_NAME . ' - Facebook"></a></li>';
                    endif;

                    if (SITE_SOCIAL_TWITTER):
                        echo '<li><a class="" target="_blank" href="https://www.twitter.com/' . SITE_SOCIAL_TWITTER . '" title="No Twitter"><img src="images/icon/icon-twitter.png" alt="' . SITE_NAME . ' - Twitter" title="' . SITE_NAME . ' - Twitter"></a></li>';
                    endif;

                    if (SITE_SOCIAL_YOUTUBE):
                        echo '<li><a class="" target="_blank" href="https://www.youtube.com/user/' . SITE_SOCIAL_YOUTUBE . '" title="No YouTube"><img src="images/icon/icon-youtube.png" alt="' . SITE_NAME . ' - YouTube" title="' . SITE_NAME . ' - YouTube"></a></li>';
                    endif;
                    ?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <main>
      <article class="page_main_doacao">      
        <div class="container">
          <div class="club_page_main_cta">
            <article class="box club_page_offer_sign" id="#offer">
              
              <h1 class="logo_text">Contribua com <br>a Aliança</h1>
 
              <div class="wc_donation_error jwc_donation_error "></div>          
                  <div class="main_mao">
                   <img src="images/mao.pnga" class="image_mao"/>
              </div>
              <h1 class="logo_text">Contribua com <br>a Aliança</h1>         
              <form action="" class="jwc_donation_form"  name="wc_send_donation" method="post" enctype="multipart/form-data">
             
                <div class="wc_donation_modal_form form-width">

                <header style="background-color: #efebe3">
                <h2 style="font-size: 1.8em;">Faça a sua doação <b>agora</b>, é bem simples!</h2>
                  </header>
        
                  <label class="box">

                  <label class="label_50">
                                    <label class="label">
                                        <span class="wc_donation_modal_legend icon-user">Nome Completo:</span>
                                        <input type="text" name="user_name" value="" placeholder="Informe seu Nome Completo:" />
                                    </label>

                  <label class="label">
                                        <span class="wc_donation_modal_legend icon-mail">Telefone:</span>
                                        <input type="email" name="user_phone" value="" placeholder="Informe seu Telefone:" />
                                    </label>
                  <div class="clear"></div>
                  </label>

                  <label class="label_50">
                                    <label class="label">
                                        <span class="wc_donation_modal_legend icon-phone">Email:</span>
                                        <input type="text" class="formPhone" name="user_email" value="" placeholder="Informe seu E-mail:" d/>
                                    </label>

                                    <label class="label">
                                        <span class="wc_donation_modal_legend icon-user-check">Seu CPF:</span>
                                        <input class="radius formCpf" name="user_document" type="text" placeholder="CPF:" />
                                    </label>
                  </label>
                  <div class="clear"></div>

                  <label class="label_50">
                                    <label class="label">
                                        <span class="wc_donation_modal_legend icon-heart">Valor para doação:</span>
                                        <input class="radius formMoney" name="donation_amount" type="text" placeholder="R$:" />
                                    </label>

                              <label class="label">
                                        <span class="wc_donation_modal_legend icon-heart">Deseja doar mensalmente?</span>
                                        <select name="recurrency" class="">
                                            <option value="" disabled="disabled" selected="selected">Selecione uma opção!</option>
                                            <option value="1">Sim</option>
                                            <option value="2">Não</option>
                                        </select>
                                    </label>
                  <div class="clear"></div>

                  <p class="al_center m_top" style="font-weight: 300; font-size: 0.8em; text-transform: uppercase;"><span class="m_right">Doe quanto você puder à partir de R$ 1</span><span class="icon-credit-card">Ou pague com cartão de crédito ou débito</span>


                    </label>
                    </label>

                          <div class="wc_donation_modal_button">
                             <button style="" class="club_call_to_action_home icon-checkmark2">DOAR AGORA</button>
                         <!-- <span class="icon-barcode">Boleto</span>--></p>
                        <img src="" alt="Aguarde, estamos processando!" title="Aguarde, estamos processando!" />
                  
                       </div>                     
                </div>
     
              </form>

              <div style="display: none;" class="wc_donation_sended jwc_donation_sended">

                <p class="h2"><span>&#10003;</span>
                  <br>Muito obrigado...</p>
                <p><b>Prezado(a) <span class="jwc_donation_sended_name">NOME</span>.  muito obrigado por realizar a sua doação</p>
            <p>Acabamos de te enviar um e-mail com as informações da sua doação. Verifique sua caixa de entrada!</p>
            </b>Estamos te redirecionando para a página de pagamento... Aguarde... </p>
                <p><em>Atenciosamente.</em></p>

              </div>
            </article>
        </div>
      </article>
    </div>
</div>
  </main>
 
  <script src="_cdn/bootstrap/bootstrap.js"></script>
</body>

</html>